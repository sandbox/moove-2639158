<?php

/**
 * @file
 * Bodyclass admin form.
 */

/** Setting admin form. */
function bodyclass_admin_form($form, &$form_state) {

  $bodyclasses = variable_get('bodyclass_configuration', 'Body classes');

  $form['bodyclass'] = array(
    '#title' => t('Body classes'),
    '#type' => 'textfield',
    '#description' => t('Enter custom classes separated by a space'),
    '#default_value' => $bodyclasses,
    '#size' => 64,
    '#maxlength' => 128,
    '#required' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Update',
    '#submit' => array('bodyclass_admin_submit'),
  );

  return $form;

}

/** Setting submission messages. */
function bodyclass_admin_submit($form, &$form_state) {

  $bodyclasses = variable_get('bodyclass_configuration', NULL);
  $bcdata = $form_state['values']['bodyclass'];
  if ($bodyclasses != $bcdata) {
    variable_set('bodyclass_configuration', $bcdata);
    drupal_set_message ('Classes added ' . $bcdata);
  }
  else {
    drupal_set_message t('Nothing to update');
  }
}
